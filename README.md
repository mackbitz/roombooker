Ofice Room booker API

Implement API for office members to book rooms in an Office space.
booking controller and model has already been added please fill in missing code to accomodate the specification.
Use any libraries you want to complete the task and preferably add some tests (API, Unit)
It´s also fine to change project structure if you feel it´s necessary.

to run whole application with MongoDB, docker and docker-compose is required to be installed, otherwise you can run MongoDB and node locally.

to start application: `docker-compose up --build`


### Specification:
* As a Office member I need to be able to book a room for meetings 
* As a Office member I want a list of bookings of a room.
* As a Office member I can only book a room for maximum of 3 hours.
* As an Office member I would like to see a list of other Office Members bookings that they own (created)


## Business rules:
### A User 
  * can only book one room at a perticular timeslot but can hold multiple bookings at once.
### A Room 
  * cannot hold more participants than capacity.
  * cannot be double booked
  * room can only be booked within the opening hours.

### A Booking 
  * contains an owner (creator) and participants
  * contains a start and end time
  * can only be created within rooms opening hours
  * cannot be more longer than 3 hours for a room.
