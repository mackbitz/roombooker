'use strict';
import express from 'express'
import mongoose from 'mongoose'
import cors from 'cors'
import bodyParser from 'body-parser'
import router from './src/routes'
const PORT = process.env.PORT || 3000
const app = express()

//connect server to mongo
mongoose.connect(
  process.env.MONGO_URI, 
  { useNewUrlParser: true, useCreateIndex: true, }
);
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
router(app)

app.listen(PORT, () => {
    console.log(`App listening on port: ${PORT}`)
}) 