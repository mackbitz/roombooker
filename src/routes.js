import {createUser, getUser, getAllUsers } from './controllers/user'
import {createRoom, getRoom, getAllRooms } from './controllers/room'


export default app => {

  // User routes
  app.route('/api/user').post(createUser)
  app.route('/api/user/:id').get(getUser)
  app.route('/api/users').get(getAllUsers)

  // Room routes
  app.route('/api/room').post(createRoom)
  app.route('/api/room/:id').get(getRoom)
  app.route('/api/rooms').get(getAllRooms)

}
