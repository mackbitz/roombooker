import mongoose from 'mongoose'
let roomSchema = new mongoose.Schema({
  name: {type: String, required: true},
  address: String,
  capacity: {type: Number, required: true}, // nr of users that can occupy a room at the same time
  opens: {type: Number, required: true}, // hour of day
  closes: {type: Number, required: true} // hour of day
})

module.exports = mongoose.model('Room', roomSchema)