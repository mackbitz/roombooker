import mongoose from 'mongoose'
const userSchema = new mongoose.Schema({
  name: {type: String, required: true},
  address: {type: String, required: true},
  dob: {type: Date, required: true},
  category: {type: [String], required: false, default : []},
  active: { type: Boolean , default: true }
})
module.exports = mongoose.model('User', userSchema)