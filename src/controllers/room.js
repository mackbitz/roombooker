import Room from '../models/room'

export const getRoom = (req, res) => {
  Room.findById(req.params.id, (err, room) => {
    if(err) {
      res.status(500).send("Oops! something wen´t wrong")
    } else {
      res.json(room)
    }
  })
}

export const getAllRooms = (req, res) => {
  Room.find({}, (err, rooms) => {
    if(err) {
      res.status(500).send("Oops! something wen´t wrong")
    } else {
      res.send(rooms)
    }
  })
}

export const createRoom = (req, res) => {
  const room = new Room(req.body)
  room.validate()
  Room.create(req.body,(err, room) => {
    if(err) {
      res.status(400).send(JSON.stringify(err))
    } else {
      res.status(201).json(room)
    }
  })
} 


