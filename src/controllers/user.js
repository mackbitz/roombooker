import User from '../models/user'

export const getUser = (req, res) => {
  User.findById(req.params.id, (err, user) => {
    if(err) {
      res.status(500).send("Oops! something wen´t wrong")
    } else {
      res.send(user)
    }
  })
}

export const getAllUsers = (req, res) => {
  User.find({}, (err,users) => {
    if(err) {
      res.status(500).send("Oops! something wen´t wrong")
    } else {
      res.send(users)
    }
  })
}

export const createUser = (req, res) => {
  const user = new User(req.body)
  user.validate()
  User.create(req.body,(err, user) => {
    if(err) {
      res.status(400).send(JSON.stringify(err))
    } else {
      res.status(201).json(user)
    }
  })
} 


